'use strict'
/*Codificacion de la clase Lista de Tareas para la logica de interactividad*/
//captura comportamiento de la actividad del usuario sobre la app
class ListaTareas {
    constructor() {
        this.tareas = JSON.parse(localStorage.getItem('tareas')); //convierte a un json

        //cuando no exista tarea que las cree por defecto
        if (!this.tareas) {
            this.tareas = [{
                    tarea: 'Aprender JavaScript',
                    completado: false
                },
                {
                    tarea: 'Aprender PHP',
                    completado: false
                },
                {
                    tarea: 'Aprender java',
                    completado: true
                }
            ];
        }

        this.cargarTareas();
        this.agregarEventListeners();
    }
    //metodos
    agregarEventListeners() {
        document.getElementById('recordatorio').addEventListener('keypress', (Evento) => {
            //pulsacion de la tecla enter
            if (evento.keyCode == 13) {
                //invoca al metodo agregar tarea y captura el texto
                this.agregarTarea(evento.target.value);
                // cuando se agrege elimina el texto
                evento.target.value = '';
            }
        });
    }

    cargarTareas() {
        localStorage.setItem('tareas', JSON.stringify(this.tareas));
        //crear codigo html
        let htmlTareas = this.tareas.reduce((html, tarea, indice) => html += this.generarHtmlTarea(tarea, indice), '');
        document.getElementById('listaTareas').innerHTML = htmlTareas;
    };

    cambiarEstadoTarea(indice) {
        this.tareas[indice].completado = !this.tareas[indice].completado;
        console.log(this.tareas[indice]);
        this.cargarTareas();
    };

    eliminarTarea(evento, indice) {
        evento.preventDefault();
        this.tareas.splice(indice, 1);
        this.cargarTareas();
    };

    generarHtmlTarea(tarea, indice) {
        return `
        <li class="list-group-item checkbox">
            <div class="container">
                <div class="row">
                    <div class="col-md-1 col-sx-1 col-lg-1 col-sm-1 checkbox"> 
                        <label>
                            <input id="cambiarEstadoTarea" type="checkbox" onchange="listaTareas.cambiarEstadoTarea(${indice})"
                            value="" class="caja-comprobacion" ${tarea.completado ? 'checked':''}>
                        </label>
                    </div>  
                    <div class="col-md-10 col-sx-10 col-lg-10 col-sm-10 texto-area ${tarea.completado ? 'tarea-completada': ''} "> 
                    ${tarea.tarea}
                    </div> 
                    
                    <div class="col-md-1 col-sx-1 col-lg-1 col-sm-1 area-icono-eliminacion"> 
                        <a class="" href="/" onclick="listaTareas.eliminarTarea(event, ${indice})">
                            
                            <svg id="eliminarTarea" data-id=${indice} class="icono-eliminacion  width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </li>
        `;
    };

    agregarTarea(tarea) {
        let padre = document.getElementById('recordatorio').parentElement;
        if (tarea !== '') {
            padre.classList.remove('has-error');

            let nuevaTarea = {
                tarea,
                completado: false
            };
            this.tareas.push(nuevaTarea);
            this.cargarTareas();
        } else {
            padre.classList.add('has-error');

        }
    };

    //cuando el usuario precione el boton
    agregarTareaClick() {
        let recordatorio = document.getElementById('recordatorio');
        let tarea = recordatorio.value; //acceder al texto de la tarea
        //si hay texto ingresa la tarea y borra el texto
        if (tarea) {
            this.agregarTarea(tarea);
            recordatorio.value = '';
        }
    }
}
//cuando se cara el doc  efectua la instancia de la tarea
let listaTareas;

window.addEventListener('load', () => {
    listaTareas = new ListaTareas();
});